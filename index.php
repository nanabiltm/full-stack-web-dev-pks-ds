<?php
abstract class hewan
{
    public $nama;
    public $darah='50';
    public $jumlahKaki;
    public $keahlian;
    
    public function __construct($nama, $jumlahKaki, $keahlian)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
    }
    
    public function GetInfoHewan()
    {
        echo "Hewan ini bernama " . $this->nama . "<br> Dengan darah = " . $this->darah . "<br> memiliki kaki berjumlah = ". 
             $this->jumlahKaki . "<br> Memiliki keahlian " . $this->keahlian . "<br>";
    }
}


class fight extends hewan
{
    public $attackPower;
    public $defensePower;

    public function SetInfoPower($attackPower, $defensePower)
    {
        $this->attackPower = $attackPower;
        $this->defensePower = $defensePower;
    }
    public function GetInfoPower()
    {
        echo "Attack Power : " .  $this->attackPower . "<br> Defense Power : " .  $this->defensePower . "<br><br>";
    }
}

class elang extends fight{
}
class harimau extends fight{
}

$elang = new elang("Elang",2,"Terbang tinggi");
$elang->GetInfoHewan();
$elang->SetInfoPower(10,5);
echo $elang->GetInfoPower();

$harimau = new harimau("Harimau",4,"Lari cepat");
$harimau->GetInfoHewan();
$harimau->SetInfoPower(7,8);
echo $harimau->GetInfoPower();



?>